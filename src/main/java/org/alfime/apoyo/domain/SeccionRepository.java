package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "secciones", path = "seccion")
public interface SeccionRepository extends CrudRepository<Seccion, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Seccion entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Seccion> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
