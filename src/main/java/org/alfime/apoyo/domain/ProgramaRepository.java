package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "programas", path = "programa")
public interface ProgramaRepository extends CrudRepository<Programa, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Programa entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Programa> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
