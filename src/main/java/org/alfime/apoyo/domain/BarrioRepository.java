package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "barrios", path = "barrio")
public interface BarrioRepository extends CrudRepository<Barrio, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(Barrio entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Barrio> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
