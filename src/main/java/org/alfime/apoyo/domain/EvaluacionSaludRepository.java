package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluacionesSalud", path = "evaluacionSalud")
public interface EvaluacionSaludRepository extends CrudRepository<EvaluacionSalud, UUID> {
    List<EvaluacionSalud> findByEvaluacionId(UUID evaluacionId);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EvaluacionSalud> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
