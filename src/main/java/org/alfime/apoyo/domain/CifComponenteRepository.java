package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "cifComponentes", path = "cifComponente")
public interface CifComponenteRepository extends CrudRepository<CifComponente, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(CifComponente entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends CifComponente> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
