package org.alfime.apoyo.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Array;
import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "asociados", path = "asociado")
public interface AsociadoRepository extends CrudRepository<Asociado, Short> {
    Asociado findByUsername(String username);

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    <S extends Asociado> S save(S entity);

    @Override
    @RestResource(exported = false)
    <S extends Asociado> Iterable<S> saveAll(Iterable<S> entities);

    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(Asociado entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Asociado> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    int countByUsername(String username);
}
