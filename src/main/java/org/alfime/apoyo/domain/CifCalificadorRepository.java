package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "cifCalificadores", path = "cifCalificador")
public interface CifCalificadorRepository extends CrudRepository<CifCalificador, Byte> {
    List<CifCalificador> findByFacilitadorIsFalse();

    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(CifCalificador entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends CifCalificador> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
