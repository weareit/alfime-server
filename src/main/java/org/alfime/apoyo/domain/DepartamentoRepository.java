package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "departamentos", path = "departamento")
public interface DepartamentoRepository extends CrudRepository<Departamento, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Departamento entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Departamento> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
