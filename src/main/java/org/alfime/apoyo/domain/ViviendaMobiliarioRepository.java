package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "viviendaMobiliarios", path = "viviendaMobiliario")
public interface ViviendaMobiliarioRepository extends CrudRepository<ViviendaMobiliario, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(ViviendaMobiliario entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends ViviendaMobiliario> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
