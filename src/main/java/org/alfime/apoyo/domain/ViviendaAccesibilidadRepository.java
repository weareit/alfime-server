package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "viviendaAccesibilidades", path = "viviendaAccesibilidad")
public interface ViviendaAccesibilidadRepository extends CrudRepository<ViviendaAccesibilidad, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(ViviendaAccesibilidad entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends ViviendaAccesibilidad> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
