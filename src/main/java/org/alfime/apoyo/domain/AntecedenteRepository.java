package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "antecedentes", path = "antecedente")
public interface AntecedenteRepository extends CrudRepository<Antecedente, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Antecedente entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Antecedente> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
