package org.alfime.apoyo.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class CifCalificador implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Byte id;

    @Column(length = 50)
    private String nombre;

    @Column(length = 2, nullable = false)
    private String codigo;

    @Column(nullable = false)
    @NotNull
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private Boolean facilitador;

    public Byte getId() {
        return id;
    }

    public void setId(Byte id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Boolean getFacilitador() {
        return facilitador;
    }

    public void setFacilitador(Boolean facilitador) {
        this.facilitador = facilitador;
    }
}
