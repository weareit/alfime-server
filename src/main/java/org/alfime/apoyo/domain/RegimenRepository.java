package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "regimenes", path = "regimen")
public interface RegimenRepository extends CrudRepository<Regimen, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Regimen entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Regimen> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
