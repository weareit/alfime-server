package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "cajasCompensacion", path = "caja-compensacion")
public interface CajaCompensacionRepository extends CrudRepository<CajaCompensacion, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(CajaCompensacion entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends CajaCompensacion> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
