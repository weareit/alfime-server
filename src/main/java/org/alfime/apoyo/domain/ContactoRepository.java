package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "contactos", path = "contacto")
public interface ContactoRepository extends CrudRepository<Contacto, Integer> {
    @Override
    @RestResource(exported = false)
    void deleteById(Integer integer);

    @Override
    @RestResource(exported = false)
    void delete(Contacto entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Contacto> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @RestResource
    List<Contacto> findByUsuarioId(@Param("id") UUID id);
}
