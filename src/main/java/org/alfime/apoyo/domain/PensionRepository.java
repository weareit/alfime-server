package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "pensiones", path = "pension")
public interface PensionRepository extends CrudRepository<Pension, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(Pension entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Pension> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
