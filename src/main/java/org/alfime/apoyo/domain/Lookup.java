package org.alfime.apoyo.domain;

public interface Lookup<T> {
    T getId();

    void setId(T id);

    String getNombre();

    void setNombre(String nombre);
}
