package org.alfime.apoyo.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class EvaluacionPsicologico implements Serializable {
    @Id
    @Column(nullable = false, columnDefinition = "char(36)")
    @Type(type = "org.hibernate.type.UUIDCharType")
    private UUID id;

    @OneToOne
    @MapsId
    private Evaluacion evaluacion;

    @Column(columnDefinition = "clob")
    private String comportamiento;

    @Column(columnDefinition = "clob")
    private String estadoEmocional;

    @Column(columnDefinition = "clob")
    private String diagnosticoSocial;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Evaluacion getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Evaluacion evaluacion) {
        this.evaluacion = evaluacion;
    }

    public String getComportamiento() {
        return comportamiento;
    }

    public void setComportamiento(String comportamiento) {
        this.comportamiento = comportamiento;
    }

    public String getEstadoEmocional() {
        return estadoEmocional;
    }

    public void setEstadoEmocional(String estadoEmocional) {
        this.estadoEmocional = estadoEmocional;
    }

    public String getDiagnosticoSocial() {
        return diagnosticoSocial;
    }

    public void setDiagnosticoSocial(String diagnosticoSocial) {
        this.diagnosticoSocial = diagnosticoSocial;
    }
}
