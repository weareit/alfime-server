package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluacionesAtenciones", path = "evaluacionAtencion")
public interface EvaluacionAtencionRepository extends CrudRepository<EvaluacionAtencion, Integer> {
    List<EvaluacionAtencion> findByEvaluacionId(UUID evaluacionId);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EvaluacionAtencion> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
