package org.alfime.apoyo.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "CIEs", path = "CIE")
public interface CIERepository extends CrudRepository<CIE, Integer> {
    Page<CIE> findByEnfermedadContains(String enfermedad, Pageable pageable);

    @Override
    @RestResource(exported = false)
    void deleteById(Integer integer);

    @Override
    @RestResource(exported = false)
    void delete(CIE entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends CIE> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
