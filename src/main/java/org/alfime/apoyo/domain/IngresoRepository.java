package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "ingresos", path = "ingreso")
public interface IngresoRepository extends CrudRepository<Ingreso, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Ingreso entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Ingreso> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
