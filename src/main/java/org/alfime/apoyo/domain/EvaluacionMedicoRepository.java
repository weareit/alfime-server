package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluacionesMedicos", path = "evaluacionMedico")
public interface EvaluacionMedicoRepository extends CrudRepository<EvaluacionMedico, UUID> {
    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EvaluacionMedico> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
