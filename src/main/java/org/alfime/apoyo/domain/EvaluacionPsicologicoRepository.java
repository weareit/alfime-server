package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluacionesPsicologicos", path = "evaluacionPsicologico")
public interface EvaluacionPsicologicoRepository extends CrudRepository<EvaluacionPsicologico, UUID> {
    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EvaluacionPsicologico> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
