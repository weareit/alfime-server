package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "discapacidades", path = "discapacidad")
public interface DiscapacidadRepository extends CrudRepository<Discapacidad, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Discapacidad entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Discapacidad> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
