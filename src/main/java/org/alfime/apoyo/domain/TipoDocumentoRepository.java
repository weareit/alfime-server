package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "tiposDocumento", path = "tipoDocumento")
public interface TipoDocumentoRepository extends CrudRepository<TipoDocumento, Byte> {
    @RestResource(exported = false)
    @Override
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(TipoDocumento entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends TipoDocumento> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
