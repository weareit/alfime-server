package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "atencionTipos", path = "atencionTipo")
public interface AtencionTipoRepository extends CrudRepository<AtencionTipo, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(AtencionTipo entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends AtencionTipo> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
