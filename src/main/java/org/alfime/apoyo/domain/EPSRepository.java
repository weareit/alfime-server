package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "EPSes", path = "eps")
public interface EPSRepository extends CrudRepository<EPS, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(EPS entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EPS> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
