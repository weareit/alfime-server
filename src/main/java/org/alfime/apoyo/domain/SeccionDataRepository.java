package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "seccionDatas", path = "seccionData")
public interface SeccionDataRepository extends CrudRepository<SeccionData, Long> {
    @SuppressWarnings("unused")
    @PreAuthorize("hasRole('ADMIN') or " +
            "(#seccionId == 1 and hasRole('PEDAGOGO')) or " +
            "(#seccionId == 2 and hasRole('DEPORTE')) or " +
            "(#seccionId == 3 and hasRole('FISIO')) or " +
            "(#seccionId == 5 and hasRole('PSICO')) or " +
            "(#seccionId == 4)")
    List<SeccionData> findAllByUsuarioIdAndSeccionId(UUID usuarioId, Byte seccionId);

    @Override
    @PostAuthorize("returnObject.isPresent() and (hasRole('ADMIN') or " +
            "(returnObject.get().seccion.id == 5 and hasRole('PSICO')) or " +
            "(returnObject.get().seccion.id == 2 and hasRole('DEPORTE')) or " +
            "(returnObject.get().seccion.id == 3 and hasRole('FISIO')) or " +
            "(returnObject.get().seccion.id == 1 and hasRole('PEDAGOGO'))" +
            ")")
    Optional<SeccionData> findById(Long aLong);

    @Override
    @PreAuthorize("hasRole('ADMIN') or " +
            "(#entity.seccion.id == 1 and hasRole('PEDAGOGO')) or " +
            "(#entity.seccion.id == 2 and hasRole('DEPORTE')) or " +
            "(#entity.seccion.id == 3 and hasRole('FISIO')) or " +
            "(#entity.seccion.id == 5 and hasRole('PSICO')) or " +
            "(#entity.seccion.id == 4)")
    <S extends SeccionData> S save(S entity);

    @Override
    @RestResource(exported = false)
    <S extends SeccionData> Iterable<S> saveAll(Iterable<S> entities);

    @Override
    boolean existsById(Long aLong);

    @Override
    @RestResource(exported = false)
    Iterable<SeccionData> findAll();

    @Override
    @RestResource(exported = false)
    Iterable<SeccionData> findAllById(Iterable<Long> longs);

    @Override
    @RestResource(exported = false)
    long count();

    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);

    @Override
    @RestResource(exported = false)
    void delete(SeccionData entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends SeccionData> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
