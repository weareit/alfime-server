package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "frecuencias", path = "frecuencia")
public interface FrecuenciaRepository extends CrudRepository<Frecuencia, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Frecuencia entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Frecuencia> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
