package org.alfime.apoyo.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Departamento {
    @Id
    private Byte id;

    @Column(length = 50, name = "departamento")
    private String nombre;

    @JsonManagedReference
    @OneToMany(mappedBy = "departamento", fetch = FetchType.LAZY)
    private List<Ciudad> ciudades;

    public Byte getId() {
        return id;
    }

    public void setId(Byte id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(List<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }
}
