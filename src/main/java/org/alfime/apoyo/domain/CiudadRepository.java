package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "ciudades", path = "ciudad")
public interface CiudadRepository extends CrudRepository<Ciudad, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(Ciudad entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Ciudad> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
