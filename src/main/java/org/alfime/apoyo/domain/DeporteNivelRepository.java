package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "deporteNiveles", path = "deporteNivel")
public interface DeporteNivelRepository extends CrudRepository<DeporteNivel, Byte> {
}
