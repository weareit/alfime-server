package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "familiares", path = "familia")
public interface FamiliaRepository extends CrudRepository<Familia, Integer> {
    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Familia> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
