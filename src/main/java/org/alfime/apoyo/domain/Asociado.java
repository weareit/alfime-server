package org.alfime.apoyo.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Asociado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Short id;

    @Column(length = 20, name = "usuario", unique = true)
    private String username;

    @Column(length = 50, name = "nombre_completo")
    private String nombre;

    @Column(length = 100)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Transient
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String passwordConfirm;

    @ManyToMany
    @JoinTable(name = "asociado_rol", joinColumns = @JoinColumn(name = "asociado_id"), inverseJoinColumns = @JoinColumn(name = "rol_id"))
    private Set<Rol> roles;

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Set<Rol> getRoles() {
        return roles;
    }

    public void setRoles(Set<Rol> roles) {
        this.roles = roles;
    }

    @PrePersist
    @PreUpdate
    void encodePassword() {
        if (password != null && password.equals(passwordConfirm)) {
            password = PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(password);
        }
    }
}
