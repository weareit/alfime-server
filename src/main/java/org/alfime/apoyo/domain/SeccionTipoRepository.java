package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "seccionTipos", path = "seccionTipo")
public interface SeccionTipoRepository extends CrudRepository<SeccionTipo, Byte> {
    @Override
    void deleteById(Byte aByte);

    @Override
    void delete(SeccionTipo entity);

    @Override
    void deleteAll(Iterable<? extends SeccionTipo> entities);

    @Override
    void deleteAll();
}
