package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "parentescos", path = "parentesco")
public interface ParentescoRepository extends CrudRepository<Parentesco, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Parentesco entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Parentesco> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
