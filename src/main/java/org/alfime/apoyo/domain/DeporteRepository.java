package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "deportes", path = "deporte")
public interface DeporteRepository extends CrudRepository<Deporte, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Deporte entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Deporte> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
