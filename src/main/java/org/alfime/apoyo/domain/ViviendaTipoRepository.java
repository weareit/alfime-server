package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "viviendaTipos", path = "viviendaTipo")
public interface ViviendaTipoRepository extends CrudRepository<ViviendaTipo, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(ViviendaTipo entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends ViviendaTipo> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
