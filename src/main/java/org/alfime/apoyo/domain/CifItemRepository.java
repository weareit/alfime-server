package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "cifItems", path = "cifItem")
public interface CifItemRepository extends CrudRepository<CifItem, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(CifItem entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends CifItem> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
