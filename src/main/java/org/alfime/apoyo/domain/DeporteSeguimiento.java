package org.alfime.apoyo.domain;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class DeporteSeguimiento extends Auditable implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    private Usuario usuario;

    @Column(columnDefinition = "timestamp", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date comienzo;

    @Column(columnDefinition = "timestamp", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Asociado asociado;

    @ManyToOne(optional = false)
    private DeporteActividad actividad;

    @ManyToOne(optional = false)
    private DeporteFuerza fuerza;

    @Column(nullable = false)
    private boolean resistencia;

    @Column(nullable = false)
    private boolean velocidad;

    @ManyToOne(optional = false)
    private DeporteNivel volumen;

    @ManyToOne(optional = false)
    private DeporteNivel intensidad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getComienzo() {
        return comienzo;
    }

    public void setComienzo(Date comienzo) {
        this.comienzo = comienzo;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Asociado getAsociado() {
        return asociado;
    }

    public void setAsociado(Asociado asociado) {
        this.asociado = asociado;
    }

    public DeporteActividad getActividad() {
        return actividad;
    }

    public void setActividad(DeporteActividad actividad) {
        this.actividad = actividad;
    }

    public DeporteFuerza getFuerza() {
        return fuerza;
    }

    public void setFuerza(DeporteFuerza fuerza) {
        this.fuerza = fuerza;
    }

    public boolean isResistencia() {
        return resistencia;
    }

    public void setResistencia(boolean resistencia) {
        this.resistencia = resistencia;
    }

    public boolean isVelocidad() {
        return velocidad;
    }

    public void setVelocidad(boolean velocidad) {
        this.velocidad = velocidad;
    }

    public DeporteNivel getVolumen() {
        return volumen;
    }

    public void setVolumen(DeporteNivel volumen) {
        this.volumen = volumen;
    }

    public DeporteNivel getIntensidad() {
        return intensidad;
    }

    public void setIntensidad(DeporteNivel intensidad) {
        this.intensidad = intensidad;
    }
}
