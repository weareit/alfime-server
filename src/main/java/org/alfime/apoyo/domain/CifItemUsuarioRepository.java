package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "cifItemUsuarios", path = "cifItemUsuario")
public interface CifItemUsuarioRepository extends CrudRepository<CifItemUsuario, Long> {
    List<CifItemUsuario> findAllByUsuarioIdAndItemCategoriaId(UUID usuarioId, Byte componenteId);

    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);

    @Override
    @RestResource(exported = false)
    void delete(CifItemUsuario entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends CifItemUsuario> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
