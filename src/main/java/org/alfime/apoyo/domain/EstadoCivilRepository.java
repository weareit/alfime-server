package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "estadosCiviles", path = "estadoCivil")
public interface EstadoCivilRepository extends CrudRepository<EstadoCivil, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(EstadoCivil entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EstadoCivil> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
