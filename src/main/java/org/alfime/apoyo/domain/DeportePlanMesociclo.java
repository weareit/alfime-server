package org.alfime.apoyo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class DeportePlanMesociclo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    Date fecha;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    DeportePlan plan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public DeportePlan getPlan() {
        return plan;
    }

    public void setPlan(DeportePlan plan) {
        this.plan = plan;
    }
}
