package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluacionesFamilias", path = "evaluacionFamilia")
public interface EvaluacionFamiliaRepository extends CrudRepository<EvaluacionFamilia, UUID> {
    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EvaluacionFamilia> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
