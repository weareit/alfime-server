package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "deporteCategorias", path = "deporteCategoria")
public interface DeporteCategoriaRepository extends CrudRepository<DeporteCategoria, Short> {
    List<DeporteCategoria> findAllByDeporteId(Byte id);
}
