package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "religiones", path = "religion")
public interface ReligionRepository extends CrudRepository<Religion, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Religion entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Religion> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
