package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluaciones", path = "evaluacion")
public interface EvaluacionRepository extends CrudRepository<Evaluacion, UUID> {
    @RestResource
    List<Evaluacion> findByUsuarioId(@Param("id") UUID id);

    @Override
    @RestResource(exported = false)
    void deleteById(UUID integer);

    @Override
    @RestResource(exported = false)
    void delete(Evaluacion entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Evaluacion> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
