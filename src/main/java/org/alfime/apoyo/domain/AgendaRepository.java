package org.alfime.apoyo.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

@Repository
@RepositoryRestResource(collectionResourceRel = "agendas", path = "agenda")
public interface AgendaRepository extends CrudRepository<Agenda, Long> {
    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);

    @Override
    @PreAuthorize("hasRole('ADMIN') or hasRole('ASIST')")
    void delete(Agenda entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Agenda> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    Set<Agenda> findAllByAsociadoId(Short asociadoId);

    Set<Agenda> findAllByAsociadoIdAndComienzoIsBetween(Short asociadoId, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date comienzo, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date fin);
}
