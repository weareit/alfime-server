package org.alfime.apoyo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class DeportePlanMicrociclo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Date fecha;

    @ManyToOne(optional = false)
    private DeporteActividad actividad;

    @ManyToOne(optional = false)
    private DeporteFuerza fuerza;

    @Column(nullable = false)
    private boolean resistencia;

    @Column(nullable = false)
    private boolean velocidad;

    @Column(nullable = false, length = 100)
    private String tecnicas;

    @Column(nullable = false, length = 100)
    private String trabajo;

    @ManyToOne(optional = false)
    private DeporteNivel volumen;

    @ManyToOne(optional = false)
    private DeporteNivel intensidad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public DeporteActividad getActividad() {
        return actividad;
    }

    public void setActividad(DeporteActividad actividad) {
        this.actividad = actividad;
    }

    public DeporteFuerza getFuerza() {
        return fuerza;
    }

    public void setFuerza(DeporteFuerza fuerza) {
        this.fuerza = fuerza;
    }

    public boolean isResistencia() {
        return resistencia;
    }

    public void setResistencia(boolean resistencia) {
        this.resistencia = resistencia;
    }

    public boolean isVelocidad() {
        return velocidad;
    }

    public void setVelocidad(boolean velocidad) {
        this.velocidad = velocidad;
    }

    public String getTecnicas() {
        return tecnicas;
    }

    public void setTecnicas(String tecnicas) {
        this.tecnicas = tecnicas;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public DeporteNivel getVolumen() {
        return volumen;
    }

    public void setVolumen(DeporteNivel volumen) {
        this.volumen = volumen;
    }

    public DeporteNivel getIntensidad() {
        return intensidad;
    }

    public void setIntensidad(DeporteNivel intensidad) {
        this.intensidad = intensidad;
    }
}
