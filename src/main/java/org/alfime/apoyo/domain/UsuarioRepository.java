package org.alfime.apoyo.domain;

import org.alfime.apoyo.domain.projection.UsuarioCompleto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "usuarios", path = "usuario", excerptProjection = UsuarioCompleto.class)
public interface UsuarioRepository extends CrudRepository<Usuario, UUID> {
    @RestResource(path = "documentos", rel = "documentos")
    List<Usuario> findByDocumento(@Param("documento") String documento);

    @Override
    @RestResource(exported = false)
    void deleteById(UUID integer);

    @Override
    @RestResource(exported = false)
    void delete(Usuario entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Usuario> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
