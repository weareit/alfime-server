package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "deporteSeguimientos", path = "deporteSeguimiento")
public interface DeporteSeguimientoRepository extends CrudRepository<DeporteSeguimiento, Long> {
}
