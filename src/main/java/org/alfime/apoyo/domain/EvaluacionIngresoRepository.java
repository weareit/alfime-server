package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluacionesIngresos", path = "evaluacionIngreso")
public interface EvaluacionIngresoRepository extends CrudRepository<EvaluacionIngreso, Integer> {
    List<EvaluacionIngreso> findByEvaluacionId(UUID evaluacionId);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EvaluacionIngreso> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
