package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "remitentes", path = "remitente")
public interface RemitenteRepository extends CrudRepository<Remitente, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(Remitente entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Remitente> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
