package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "familiaTipos", path = "familiaTipo")
public interface FamiliaTipoRepository extends CrudRepository<FamiliaTipo, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(FamiliaTipo entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends FamiliaTipo> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
