package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "generos", path = "genero")
public interface GeneroRepository extends CrudRepository<Genero, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Genero entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Genero> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
