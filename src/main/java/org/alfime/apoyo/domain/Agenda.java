package org.alfime.apoyo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(indexes = {
        @Index(columnList = "asociado_id, comienzo")
})
public class Agenda implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, nullable = false)
    private String titulo;

    @Column(columnDefinition = "timestamp", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date comienzo;

    @Column(columnDefinition = "timestamp", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @ManyToOne(optional = false)
    private Asociado asociado;

    @ManyToMany()
    @JoinTable(name = "agenda_usuario")
    private Set<Usuario> usuarios;

    @Column(nullable = false)
    private boolean multiples = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getComienzo() {
        return comienzo;
    }

    public void setComienzo(Date comienzo) {
        this.comienzo = comienzo;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Asociado getAsociado() {
        return asociado;
    }

    public void setAsociado(Asociado asociado) {
        this.asociado = asociado;
    }

    public Set<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public boolean isMultiples() {
        return multiples;
    }

    public void setMultiples(boolean multiples) {
        this.multiples = multiples;
    }
}
