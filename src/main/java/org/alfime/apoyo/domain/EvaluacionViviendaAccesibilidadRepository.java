package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RepositoryRestResource(collectionResourceRel = "evaluacionViviendaAccesibilidades", path = "evaluacionViviendaAccesibilidad")
public interface EvaluacionViviendaAccesibilidadRepository extends CrudRepository<EvaluacionViviendaAccesibilidad, Integer> {
    List<EvaluacionViviendaAccesibilidad> findByEvaluacionId(UUID evaluacionId);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends EvaluacionViviendaAccesibilidad> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
