package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.*;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.UUID;

@Projection(name = "resumen", types = {Evaluacion.class})
public interface EvaluacionResumen {
    UUID getId();

    boolean getAceptado();

    List<Programa> getPlan();

    Remitente getRemitente();

    String getObservaciones();

    Usuario getUsuario();

    Asociado getEvaluador();

    String getConclusiones();

    List<Programa> getAspiracion();
}
