package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.Agenda;
import org.alfime.apoyo.domain.Asociado;
import org.alfime.apoyo.domain.Usuario;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Projection(name = "usuarios", types = {Agenda.class})
public interface AgendaUsuario {
    Long getId();

    String getTitulo();

    Date getComienzo();

    Date getFin();

    boolean getMultiples();

    Set<UsuarioId> getUsuarios();

    interface UsuarioId {
        UUID getId();

        String getDocumento();

        String getNombre();

        String getSegundoNombre();

        String getApellidoPaterno();

        String getApellidoMaterno();
    }
}
