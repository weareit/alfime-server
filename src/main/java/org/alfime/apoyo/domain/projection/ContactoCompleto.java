package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.Barrio;
import org.alfime.apoyo.domain.Contacto;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "completo", types = {Contacto.class})
public interface ContactoCompleto {
    Integer getId();

    Boolean getActual();

    Barrio getBarrio();

    String getTelefono1();

    String getTelefono2();

    String getDireccion();
}
