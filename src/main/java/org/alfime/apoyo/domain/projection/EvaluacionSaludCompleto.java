package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.*;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "completo", types = {EvaluacionSalud.class})
public interface EvaluacionSaludCompleto {
    UUID getId();

    Regimen getRegimen();

    EPS getEps();

    Pension getFondoPension();

    CajaCompensacion getCajaCompensacion();
}
