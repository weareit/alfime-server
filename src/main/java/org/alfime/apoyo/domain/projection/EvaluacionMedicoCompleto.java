package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.CIE;
import org.alfime.apoyo.domain.EvaluacionMedico;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.UUID;

@Projection(name = "completo", types = {EvaluacionMedico.class})
public interface EvaluacionMedicoCompleto {
    UUID getId();

    String getRecomendaciones();

    String getSinopsis();

    List<CIE> getDiagnostico();

    List<CIE> getFamiliar();
}
