package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.Genero;
import org.alfime.apoyo.domain.Usuario;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;
import java.util.UUID;

@Projection(name = "summary", types = {Usuario.class})
public interface UsuarioSummary {
    UUID getId();

    @Value("#{target.nombre + ' ' + target.segundoNombre + ' ' + target.apellidoPaterno + ' ' + target.apellidoMaterno}")
    String getNombreCompleto();

    @Value("#{target.ciudad.nombre.toLowerCase() + ', ' + target.ciudad.departamento.nombre.toLowerCase()}")
    String getCiudadDepartamento();

    @Value("#{target.tipoDocumento.nombre + ' ' + target.documento}")
    String getDocumentoCompleto();

    Genero getGenero();

    Date getNacimiento();
}
