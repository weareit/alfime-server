package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.*;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;
import java.util.UUID;

@Projection(name = "completo", types = {EvaluacionFamilia.class})
public interface EvaluacionFamiliaCompleto {
    UUID getId();

    TipoDocumento getPadreDocumento();

    FamiliaTipo getFamiliaTipo();

    String getCalidadRelaciones();

    String getPadreIdentificacion();

    String getPadreNombre();

    TipoDocumento getMadreDocumento();

    String getMadreIdentificacion();

    String getMadreNombre();

    String getActividadesRecreativas();

    Boolean getActitud1();

    Boolean getActitud2();

    Boolean getActitud3();

    Boolean getActitud4();

    Boolean getActitud5();

    Boolean getActitud6();

    List<Familia> getMiembros();

    EstadoCivil getEstadoCivil();

    Religion getReligion();
}
