package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.SeccionData;
import org.alfime.apoyo.domain.SeccionTipo;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name = "lista", types = {SeccionData.class})
public interface SeccionDataList {
    Byte getId();

    String getCreatedBy();

    Date getFecha();

    Long getCreatedDate();

    Long getModifiedDate();

    SeccionTipo getTipo();
}
