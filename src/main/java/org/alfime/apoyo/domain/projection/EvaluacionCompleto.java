package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.Evaluacion;
import org.alfime.apoyo.domain.Programa;
import org.alfime.apoyo.domain.Remitente;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Projection(name = "evaluacion", types = {Evaluacion.class})
public interface EvaluacionCompleto {
    UUID getId();

    Date getFecha();

    boolean getAceptado();

    String getObservaciones();

    String getConclusiones();

    List<Programa> getPlan();

    List<Programa> getAspiracion();

    Remitente getRemitente();
}
