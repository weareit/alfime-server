package org.alfime.apoyo.domain.projection;

import org.alfime.apoyo.domain.Asociado;
import org.alfime.apoyo.domain.Rol;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Set;

@Projection(name = "roles", types = {Asociado.class})
@PreAuthorize("hasRole('ADMIN')")
public interface AsociadoRoles {
    Short getId();

    String getUsername();

    String getNombre();

    Set<Rol> getRoles();
}
