package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RepositoryRestResource(collectionResourceRel = "viviendaMateriales", path = "viviendaMaterial")
public interface ViviendaMaterialRepository extends CrudRepository<ViviendaMaterial, Byte> {
    List<ViviendaMaterial> findAllBySeccion(Byte seccionId);

    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(ViviendaMaterial entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends ViviendaMaterial> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
