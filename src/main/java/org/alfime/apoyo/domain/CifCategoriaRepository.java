package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "cifCategorias", path = "cifCategoria")
public interface CifCategoriaRepository extends CrudRepository<CifCategoria, Byte> {
    @Override
    void deleteById(Byte aByte);

    @Override
    void delete(CifCategoria entity);

    @Override
    void deleteAll(Iterable<? extends CifCategoria> entities);

    @Override
    void deleteAll();
}
