package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "meses", path = "mes")
public interface MesRepository extends CrudRepository<Mes, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(Mes entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Mes> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
