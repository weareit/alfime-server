package org.alfime.apoyo.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "roles", path = "rol")
public interface RolRepository extends CrudRepository<Rol, Short> {
    @Override
    @RestResource(exported = false)
    void deleteById(Short aShort);

    @Override
    @RestResource(exported = false)
    void delete(Rol entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Rol> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Query("select distinct r.asociados from Rol r where r.id in (:ids)")
    Iterable<Asociado> findAllByIdIn(Short[] ids);
}
