package org.alfime.apoyo.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(collectionResourceRel = "responsabilidades", path = "responsabilidad")
public interface ResponsabilidadRepository extends CrudRepository<Responsabilidad, Byte> {
    @Override
    @RestResource(exported = false)
    void deleteById(Byte aByte);

    @Override
    @RestResource(exported = false)
    void delete(Responsabilidad entity);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Responsabilidad> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
