package org.alfime.apoyo.resources;

import org.alfime.apoyo.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://127.0.0.1:3000")
@RestController
public class LookUpResource {
    @Autowired
    private BarrioRepository barrioRepository;

    @Autowired
    private CajaCompensacionRepository cajaCompensacionRepository;

    @Autowired
    private CiudadRepository ciudadRepository;

    @Autowired
    private DepartamentoRepository departamentoRepository;

    @Autowired
    private DiscapacidadRepository discapacidadRepository;

    @Autowired
    private EPSRepository epsRepository;

    @Autowired
    private EscolaridadRepository escolaridadRepository;

    @Autowired
    private EstadoCivilRepository estadoCivilRepository;

    @Autowired
    private FamiliaTipoRepository familiaTipoRepository;

    @Autowired
    private FrecuenciaRepository frecuenciaRepository;

    @Autowired
    private GeneroRepository generoRepository;

    @Autowired
    private IngresoRepository ingresoRepository;

    @Autowired
    private OcupacionRepository ocupacionRepository;

    @Autowired
    private ParentescoRepository parentescoRepository;

    @Autowired
    private PensionRepository pensionRepository;

    @Autowired
    private ProgramaRepository programaRepository;

    @Autowired
    private RegimenRepository regimenRepository;

    @Autowired
    private RemitenteRepository remitenteRepository;

    @Autowired
    private ResponsabilidadRepository responsabilidadRepository;

    @Autowired
    private TipoDocumentoRepository tipoDocumentoRepository;

    @GetMapping("/barrios")
    public Iterable<Barrio> retrieveAllBarrios() {
        return barrioRepository.findAll();
    }

    @GetMapping("/caja-compensacion")
    public Iterable<CajaCompensacion> retrieveAllCajasCompensacion() {
        return cajaCompensacionRepository.findAll();
    }

    @GetMapping("/ciudades")
    public Iterable<Ciudad> retrieveAllCiudades() {
        return ciudadRepository.findAll();
    }

    @GetMapping("/departamentos")
    public Iterable<Departamento> retrieveAllDepartamentos() {
        return departamentoRepository.findAll();
    }

    @GetMapping("/discapacidades")
    public Iterable<Discapacidad> retrieveAllDiscapacidades() {
        return discapacidadRepository.findAll();
    }

    @GetMapping("/documento-tipos")
    public Iterable<TipoDocumento> retrieveAllTipoDocumento() {
        return tipoDocumentoRepository.findAll();
    }

    @GetMapping("/eps")
    public Iterable<EPS> retrieveAllEPSs() {
        return epsRepository.findAll();
    }

    @GetMapping("/escolaridades")
    public Iterable<Escolaridad> retrieveAllEscolaridades() {
        return escolaridadRepository.findAll();
    }

    @GetMapping("/estados-civiles")
    public Iterable<EstadoCivil> retrieveAllEstadosCiviles() {
        return estadoCivilRepository.findAll();
    }

    @GetMapping("/frecuencias")
    public Iterable<Frecuencia> retrieveAllFrecuencias() {
        return frecuenciaRepository.findAll();
    }

    @GetMapping("/familia-tipos")
    public Iterable<FamiliaTipo> retrieveAllFamiliaTipo() {
        return familiaTipoRepository.findAll();
    }

    @GetMapping("/generos")
    public Iterable<Genero> retrieveAllGeneros() {
        return generoRepository.findAll();
    }

    @GetMapping("/ingresos")
    public Iterable<Ingreso> retrieveAllIngresos() {
        return ingresoRepository.findAll();
    }

    @GetMapping("/ocupaciones")
    public Iterable<Ocupacion> retrieveAllOcupaciones() {
        return ocupacionRepository.findAll();
    }

    @GetMapping("/parentescos")
    public Iterable<Parentesco> retrieveAllParentesco() {
        return parentescoRepository.findAll();
    }

    @GetMapping("/pensiones")
    public Iterable<Pension> retrieveAllPension() {
        return pensionRepository.findAll();
    }

    @GetMapping("/programas")
    public Iterable<Programa> retrieveAllPrograma() {
        return programaRepository.findAll();
    }

    @GetMapping("/regimenes")
    public Iterable<Regimen> retrieveAllRegimenes() {
        return regimenRepository.findAll();
    }

    @GetMapping("/remitentes")
    public Iterable<Remitente> retrieveAllRemitentes() {
        return remitenteRepository.findAll();
    }

    @GetMapping("/responsabilidades")
    public Iterable<Responsabilidad> retrieveAllResponsabilidades() {
        return responsabilidadRepository.findAll();
    }
}
