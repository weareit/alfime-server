package org.alfime.apoyo.user;

import org.alfime.apoyo.domain.Asociado;
import org.alfime.apoyo.domain.AsociadoRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private AsociadoRepository applicationUserRepository;

    public UserDetailsServiceImpl(AsociadoRepository applicationUserRepository) {
        this.applicationUserRepository = applicationUserRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Asociado applicationUser = applicationUserRepository.findByUsername(username);

        if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
        }

        return new User(applicationUser.getUsername(), applicationUser.getPassword(), true, true, true, true, this.getRoles(applicationUser));
    }

    private Collection<? extends GrantedAuthority> getRoles(Asociado applicationUser) {
        return applicationUser.getRoles().stream().map(rol -> new SimpleGrantedAuthority("ROLE_" + rol.getCodigo())).collect(Collectors.toList());
    }
}
